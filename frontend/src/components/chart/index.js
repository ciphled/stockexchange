
import React, { useEffect, useState } from 'react'

import { scaleTime } from 'd3-scale'
import { format } from 'd3-format'
import { timeFormat } from 'd3-time-format'

import { ChartCanvas, Chart } from 'react-stockcharts'
import { XAxis, YAxis } from 'react-stockcharts/lib/axes'
import {
  CrossHairCursor,
  EdgeIndicator,
  CurrentCoordinate,
  MouseCoordinateX,
  MouseCoordinateY
} from 'react-stockcharts/lib/coordinates'
import { curveMonotoneX } from 'd3-shape'

import { OHLCTooltip } from 'react-stockcharts/lib/tooltip'
import { fitWidth } from 'react-stockcharts/lib/helper'

import { AreaSeries } from 'react-stockcharts/lib/series'
import { Spin } from 'antd'

import { createVerticalLinearGradient, hexToRGBA } from 'react-stockcharts/lib/utils'
import moment from 'moment'

const priceGradient = createVerticalLinearGradient([
  { stop: 0, color: hexToRGBA('#b5d0ff', 0.2) },
  { stop: 0.7, color: hexToRGBA('#6fa4fc', 0.4) },
  { stop: 1, color: hexToRGBA('#4286f4', 0.8) }
])

const volumeGradient = createVerticalLinearGradient([
  { stop: 0, color: 'rgba(200,0,200,0.1)' },
  { stop: 0.7, color: 'rgba(200,0,200,0.3)' },
  { stop: 1, color: 'rgba(200,0,200,0.6)' }
])

const IntradayChart = React.forwardRef((props, ref) => {
  const { width = 500, ratio = 2, ticker = 'AAPL' } = props
  const [data, setData] = useState([])

  useEffect(() => {
    const conn = new window.WebSocket('ws://localhost:8080/ws?sym=' + ticker)
    conn.onmessage = function (evt) {
      const message = JSON.parse(evt.data)
      const am = message.Price
      const vol = message.Volume
      const date = new Date(message.Time)
      const event = {
        open: am,
        close: am,
        low: am,
        high: am,
        date,
        volume: vol
      }
      setData(d => [...d, event])
    }
    return () => conn.close()
  }, [ticker])

  const xAccessor = d => {
    return d?.date
  }

  if (data.length < 3) {
    return <Spin style={{ margin: '120px 0' }} />
  }

  const start = moment(new Date())
  start.add(-1, 'minutes')
  const end = moment(new Date())
  end.add(1, 'minutes')

  const xExtents = [start, end]

  return (
    <ChartCanvas
      height={400}
      ratio={ratio}
      width={width}
      margin={{ left: 80, right: 80, top: 10, bottom: 30 }}
      seriesName={ticker}
      data={data}
      xScale={scaleTime()}
      xAccessor={xAccessor}
      displayXAccessor={xAccessor}
      xExtents={xExtents}
      ref={ref}
    >
      <Chart
        id={1}
        yExtents={[d => [d.high * 1.1, d.low * 0.9]]}
      >
        <XAxis
          axisAt='bottom'
          orient='bottom'
          panEvent
          zoomEvent
        />
        <YAxis axisAt='right' orient='right' ticks={5} />

        <MouseCoordinateX
          rectWidth={60}
          at='bottom'
          orient='bottom'
          displayFormat={timeFormat('%H:%M:%S')}
        />
        <MouseCoordinateY
          at='right'
          orient='right'
          displayFormat={format('.2f')}
        />

        <AreaSeries
          yAccessor={d => d.close}
          fill='url(#MyGradient)'
          strokeWidth={2}
          interpolation={curveMonotoneX}
          canvasGradient={priceGradient}
        />
        <OHLCTooltip origin={[-40, 0]} xDisplayFormat={timeFormat('%Y-%m-%d %H:%M:%S')} />
        <EdgeIndicator
          itemType='last' orient='right' edgeAt='right'
          yAccessor={d => d.close} displayFormat={format('.4s')} fill='#0F0F0F'
        />
      </Chart>
      <Chart
        id={2}
        yExtents={d => [0, d.volume]}
        height={100}
        origin={(w, h) => [0, h - 100]}
      >
        <YAxis axisAt='left' orient='left' ticks={5} tickFormat={format('.2s')} />

        <MouseCoordinateY
          at='left'
          orient='left'
          displayFormat={format('.4s')}
        />

        <AreaSeries
          yAccessor={d => d.volume}
          fill='url(#MyGradient)'
          strokeWidth={2}
          interpolation={curveMonotoneX}
          canvasGradient={volumeGradient}
        />
        <CurrentCoordinate yAccessor={d => d.volume} fill='#9B0A47' />
        <EdgeIndicator
          itemType='last' orient='left' edgeAt='left'
          yAccessor={d => d.volume} displayFormat={format('.4s')} fill='#0F0F0F'
        />
      </Chart>
      <CrossHairCursor />
    </ChartCanvas>
  )
})

export default fitWidth(IntradayChart)
