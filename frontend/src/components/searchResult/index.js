
function SearchResult ({ result }) {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between'
      }}
    >
      <span>
        {result.ticker}
      </span>
      <span>
        {result.long}
      </span>
    </div>
  )
}

export default SearchResult
