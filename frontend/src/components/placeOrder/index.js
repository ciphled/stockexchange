
import { Modal, Button, Form, Input } from 'antd'
import { useState } from 'react'
import { addOrder } from '../../api/stock'

function PlaceOrder ({ ticker }) {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [type, setType] = useState('Buy')
  const showModal = (type) => {
    setIsModalVisible(true)
    setType(type)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
  }
  const handleSubmit = (values) => {
    const order = {
      Price: parseInt(values.Price),
      Size: parseInt(values.Size),
      Id: 1, // TODO: add sequence in Go be
      Type: 'A',
      Sym: ticker,
      Side: type[0]
    }

    addOrder(order)
  }

  return (
    <>
      <Button type='primary' onClick={() => showModal('Buy')}>
        Buy
      </Button>
      <Button type='primary' onClick={() => showModal('Sell')}>
        Sell
      </Button>
      <Modal title={`${type} ${ticker} (Limit Order)`} footer={null} onCancel={handleCancel} visible={isModalVisible}>
        <Form
          layout='vertical'
          onFinish={handleSubmit}
        >
          <Form.Item
            label='Volume'
            name='Size'
            required tooltip='How many shares?'
            rules={[{ required: true, message: 'You must specify a volume' }]}
          >
            <Input type='number' placeholder='Number of units' />
          </Form.Item>
          <Form.Item
            label='Unit Price (AUD)'
            name='Price'
            required
            rules={[{ required: true, message: 'You must specify a price' }]}
            tooltip='How much per share?'
          >
            <Input type='number' placeholder='Price per unit' />
          </Form.Item>
          <Form.Item>
            <div style={{ display: 'inline-grid', gridAutoFlow: 'column', gap: 5 }}>
              <Button type='primary' htmlType='submit'>Submit</Button>
              <Button type='danger' htmlType='reset' onClick={() => setIsModalVisible(false)}>cancel</Button>
            </div>
          </Form.Item>
        </Form>

      </Modal>
    </>
  )
}

export default PlaceOrder
