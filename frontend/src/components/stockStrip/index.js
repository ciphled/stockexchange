import Ticker from '../ticker'
import Strip from 'react-ticker'

function StockStrip () {
  const sample = [
    { ticker: 'AAPL', change: 100 },
    { ticker: 'MSFT', change: -112 },
    { ticker: 'VVNC', change: 1020 },
    { ticker: 'SPH', change: -123 },
    { ticker: 'MSQ', change: -323 },
    { ticker: 'FTY', change: 754 },
    { ticker: 'UTR', change: 234 },
    { ticker: 'MWQ', change: -234 },
    { ticker: 'VVQ', change: 574 }
  ]
  return (
    <Strip>
      {({ index }) => (
        <Ticker {...sample[index % sample.length]} />
      )}
    </Strip>
  )
}

export default StockStrip
