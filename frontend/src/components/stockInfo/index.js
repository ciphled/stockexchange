
import { Skeleton, Divider, Tag } from 'antd'
import Title from 'antd/lib/typography/Title'
import { useEffect, useState } from 'react'
import { getStockOverview } from '../../api/stock'

function StockInfo ({ sym }) {
  const [data, setData] = useState()
  useEffect(() => {
    getStockOverview(sym).then(d => {
      setData(d)
    })
  }, [sym])
  if (!data) {
    return (
      <Skeleton active />
    )
  }
  return (
    <div style={{ textAlign: 'left' }}>
      <Title level={4}>About</Title>
      {data.Description}
      <Divider />
      <div style={{ textAlign: 'left' }}>
        {
          Object.entries(data).map(([k, v]) => {
            if (k === 'Description') return null
            return (
              <Tag key={k}>{`${k}: `}<b>{v}</b></Tag>
            )
          })
        }
      </div>
    </div>
  )
}

export default StockInfo
