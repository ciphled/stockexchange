import {
  CaretUpOutlined,
  CaretDownOutlined
} from '@ant-design/icons'

import { TickerContainer } from './index.styles'

function Ticker ({ ticker, change }) {
  const Icon = () => {
    if (change > 0) {
      return <CaretUpOutlined />
    }
    return <CaretDownOutlined />
  }
  return (
    <TickerContainer>
      <span>
        {ticker}
      </span>
      <span style={{ color: change > 0 ? 'green' : 'red' }}>
        <Icon />
        {change}
      </span>
    </TickerContainer>
  )
}

export default Ticker
