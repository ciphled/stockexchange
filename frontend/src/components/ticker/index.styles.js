import styled from 'styled-components'

export const TickerContainer = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 0 5px;
    font-size: 25px;
`
