import React from 'react'
import { Switch, Route } from 'react-router-dom'
import * as ROUTES from './constants'

import { Home, Stock } from '../pages'

function Routes () {
  return (
    <Switch>
      <Route exact path={`${ROUTES.stock}/:sym`}>
        <Stock />
      </Route>
      <Route path={ROUTES.home}>
        <Home />
      </Route>
    </Switch>
  )
}

export { ROUTES }

export default Routes
