import { Plate, MegaTitle, SearchContainer, Container } from './index.styles'
import { SearchResult } from '../../components'

import { Input, AutoComplete } from 'antd'
import { useState } from 'react'
import { ROUTES } from '../../routes'
import { useHistory } from 'react-router-dom'
import StockStrip from '../../components/stockStrip'

const stocks = [
  { ticker: 'AAPL', long: 'Apple Inc.' },
  { ticker: 'MSFT', long: 'Microsoft Corp.' }
]
const includes = (o, query) => {
  query = query.toLowerCase()
  for (const e of Object.entries(o)) {
    if (e[1].toLowerCase().includes(query)) { return true }
  }
  return false
}
const searchResult = (query) => {
  return stocks
    .filter(o => includes(o, query))
    .map(o => ({
      value: o.ticker,
      label: (
        <SearchResult result={o} />
      )
    }))
}

function Home () {
  const [options, setOptions] = useState([])
  const history = useHistory()
  const handleSearch = (value) => {
    setOptions(value ? searchResult(value) : [])
  }

  const onSelect = (value) => {
    history.push(`${ROUTES.stock}/${value}`)
  }

  return (
    <Container>
      <Plate>
        <MegaTitle>Stonks</MegaTitle>
        <SearchContainer>
          <AutoComplete
            dropdownMatchSelectWidth={440}
            style={{ transform: 'scale(1.5)' }}
            options={options}
            onSelect={onSelect}
            onSearch={handleSearch}
          >
            <Input.Search size='large' placeholder='Search' enterButton />
          </AutoComplete>
        </SearchContainer>
      </Plate>
      <StockStrip />
    </Container>
  )
}

export default Home
