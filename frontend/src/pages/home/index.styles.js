import styled from 'styled-components'

export const Plate = styled.div`
    display: grid;
    grid-template-rows: 1fr 1fr;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    justify-content: center;
    align-items: flex-end;
    flex-direction: column;
    padding: 20px;
    background: linear-gradient(0deg, rgba(38,209,212,1) 0%, rgba(223,255,169,1) 100%); 
`

export const Container = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`

export const MegaTitle = styled.h1`
    font-size: 80px;
    color: black;
`

export const SearchContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    height: 100%;
    width: 100%;
`
