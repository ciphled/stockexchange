import {
  useHistory,
  useParams
} from 'react-router-dom'

import { StockInfo, PlaceOrder, Chart } from '../../components'
import { PageHeader, Tabs } from 'antd'
import { ROUTES } from '../../routes'

const { TabPane } = Tabs

const extraContent = (ticker) => {
  return (
    <PlaceOrder ticker={ticker} />
  )
}

const Content = ({ children, extra }) => (
  <div>
    <div>{children}</div>
    <div>{extra}</div>
  </div>
)

function Stock () {
  const { sym } = useParams()
  const history = useHistory()

  return (
    <>
      <PageHeader
        onBack={() => history.push(ROUTES.home)}
        title={sym}
        extra={extraContent(sym)}
        footer={
          <Tabs defaultActiveKey='1'>
            <TabPane tab='Chart' key='1'>
              <Chart ticker={sym} />
            </TabPane>
            <TabPane tab='Depth' key='2' disabled>
              Depth
            </TabPane>
            <TabPane tab='Order History' key='3' disabled>
              Order History
            </TabPane>
          </Tabs>
        }
      >
        <Content>
          <StockInfo sym={sym} />
        </Content>
      </PageHeader>
    </>
  )
}

export default Stock
