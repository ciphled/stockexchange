import Home from './home'
import Stock from './stock'

export {
  Home,
  Stock
}
