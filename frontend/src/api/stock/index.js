import axios from 'axios'

const key = 'F22A6VQBIRLBA6QP'
const avBase = `https://www.alphavantage.co/query?apikey=${key}`
const goBase = 'http://localhost:8080'

export async function getStockOverview (ticker) {
  const keys = ['Name', 'AssetType', 'Sector', 'Industry', 'Country', 'Exchange', 'Description', 'Currency']
  const overviewURL = `${avBase}&function=OVERVIEW`
  const { data } = await axios.get(`${overviewURL}&symbol=${ticker}`)
  const result = {}
  for (const key of keys) {
    result[key] = data[key]
  }
  return result
}

export async function addOrder (payload) {
  // Request the golang backend
  const ordersUrl = `${goBase}/orders`
  await axios.post(ordersUrl, payload)
}
