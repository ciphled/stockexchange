# Stock Exchange

High performance stock exchange built from scratch using Go, C++ and React entirely by Winton Nathan-Roberts. React interacts with a Matching Engine via a Golang web server interface over TCP/IP and websockets, which communicates with the Matching Engine through ZeroMQ message passing. The Matching Engine is an efficient C++ implementation which matches buyers with sellers, facilitating stock order fulfilment. For simplicity, we focus only implement `Add` orders as a proof of concept. The price of a stock is simply taken as the most competitive price.

![alt text](https://upload.wikimedia.org/wikipedia/commons/1/14/Order_book_depth_chart.gif "Depth")


## Architecture

The architecture is detailed in the diagram below, which should be self explanatory. ZMQ stands for ZeroMQ, which is a service for efficient queued message passing between services offering high speed and  reliability.


![alt text](./architecture.png "Depth")

## Video Demo 

[YouTube Video](https://www.youtube.com/watch?v=4-C8JPqt8gk)


## Build and Run
Ensure you have `node`, `yarn`, `golang` and a C++ compiler like `clang` installed on your machine, then see each subdirectory for deployment instructions.
Once all services are running, visit the Stonks stock exchange at `http://localhost:3000` in your browser.

## Matching Engine

The most competitive buy orders (highest price) are matched with the most competitive sell orders (lowest price), which naturally represents how the stock would be sold at an auction house. One question remains. How should we deal with ties in price?

### Continuous Trading

We will focus on continuous trading which is the most popular form of trading, and attempt to implement a Matching Algorithm which satisfies the Price/Time Priority (FIFO) policy:

```
Sell orders with the same price are filled in ascending order of time (hence FIFO).
```

### Data structures

- Each side is a `priority queue` that's max or min top for the buy and sell sides respectively.
- The queues are sorted by a custom comparator which is by price, and then time in the case of a tie. In pseudo-code:

```python
def comp(orderA, orderB):
    if orderA.price != orderB.price:
        return orderA.price-orderB.price
    return orderA.time-orderB.time
```

With these data structure, the matching algorithm will be efficient and easy to implement with a time complexity of `O(log n)` per order where `n` is the number of existing orders. `O(n)` space complexity, which is optimal.

### Devil is in the details

There are a few details we haven't talked about.

- Partial order fulfilment is possible. The most competitive buy order will be filled until no sell orders with a feasible price remain. It's possible that the order won't fill, so it is simply added back to the buy queue.
- When should the state update? When a new order comes in, where it is then immediately processed. There's no other case where the depth could change.


## Todo

- Add docker for better portability (easy build/run)
- Use C++ templates to create a generic Orders datastructure for the matching algorithm to operate on
- Add different Order types