# Golang Webserver

The C++ matching engine is in `./MatchingEngine`. See it for build instructions.

## Build

Install missing dependencies.

```
 go get 'github.com/gorilla/websocket'
 go get 'github.com/pebbe/zmq4'
 go mod tidy
```

## Run 

```
go run .
```