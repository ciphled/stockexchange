package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

const (
	writeWait = 10 * time.Second
)

type Order struct {
	Price int
	Size  int
	Id    int
	Type  string
	Sym   string
	Side  string
}

type Client struct {
	me   *MatchingEngine // me for the ticker
	conn *websocket.Conn // ws between client/server
	send chan *State     // channel of price updates from ME
}

func (c *Client) writePump() {
	defer func() {
		c.conn.WriteMessage(websocket.CloseMessage, []byte{})
		c.conn.Close()
	}()
	for state := range c.send {
		c.conn.SetWriteDeadline(time.Now().Add(writeWait))

		err := c.conn.WriteJSON(*state)
		if err != nil {
			return
		}

		// Clear out the queue of state updates
		for i := 0; i < len(c.send); i++ {
			state := <-c.send
			if err := c.conn.WriteJSON(*state); err != nil {
				return
			}
		}
	}

}

func handleChart(mkt *Market, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	params := r.URL.Query()
	sym := params.Get("sym")
	// get corresponding market
	me := mkt.getMatchingEngine(sym)

	client := &Client{me: me, conn: conn, send: make(chan *State, 256)}
	me.register <- client

	go client.writePump() // handle pump state updates to client over ws
}
