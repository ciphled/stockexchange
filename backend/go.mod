module hft.com/matching

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/pebbe/zmq4 v1.2.7
)
