#ifndef _ORDER_H
#define _ORDER_H

#include "../Printable/I_Printable.h"
#include <string>

class Order : public I_Printable
{
    friend class OrderBook;
    friend bool operator<(const Order &a, const Order &b);

private:
    std::string symbol;
    char side;
    uint64_t id;
    uint64_t volume;
    int price;

public:
    Order(std::string symbol, char side, uint64_t id, uint64_t volume, int price);
    Order(const Order &);

    void print(std::ostream &) const override;

    std::string getSymbol();
    char getSide();
    int getPrice();
    void setPrice(int);
    uint64_t getVolume();
    void setVolume(uint64_t);
    uint64_t getId();
};

#endif
