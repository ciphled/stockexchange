#include "Order.h"
#include <iomanip>

using namespace std;

Order::Order(string symbol, char side, uint64_t id, uint64_t volume, int price)
    : symbol(symbol), side(side), id(id), volume(volume), price(price) {}

Order::Order(const Order &other)
    : symbol(other.symbol), side(other.side), id(other.id), volume(other.volume), price(other.price) {}

void Order::print(ostream &os) const
{
    os << symbol << ", ";
    os << side << ", ";
    os << id << "id, ";
    os << volume << " @ $";
    os << setprecision(2) << fixed << price / 10000;
}

string Order::getSymbol()
{
    return symbol;
}
char Order::getSide()
{
    return side;
}

int Order::getPrice()
{
    return price;
}

void Order::setPrice(int price)
{
    this->price = price;
}

uint64_t Order::getVolume()
{
    return volume;
}

void Order::setVolume(uint64_t volume)
{
    this->volume = volume;
}

uint64_t Order::getId()
{
    return id;
}

bool operator<(const Order &a, const Order &b)
{
    int flip = (a.side == 'B') ? 1 : -1;
    if (a.price != b.price)
    {
        return flip * (a.price - b.price) > 0;
    }
    return flip * (a.id - b.id) > 0;
}