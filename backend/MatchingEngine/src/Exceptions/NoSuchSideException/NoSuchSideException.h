#ifndef _NO_SUCH_SIDE_EXCEPTION_H
#define _NO_SUCH_SIDE_EXCEPTION_H
#include <exception>

class NoSuchSideException : public std::exception
{
public:
    virtual const char *what() const noexcept;
};

#endif