#include "NoSuchSideException.h"

const char *NoSuchSideException::what() const noexcept
{
    return "There's no such side. Must be either buy or ask.";
}