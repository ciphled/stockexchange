#include "Market.h"

using namespace std;

void Market::addOrderBook(string &sym)
{
    // doesn't overwrite existing OB
    auto p = make_unique<OrderBook>(sym);
    booksBySymbol.emplace(sym, std::move(p));
}

OrderBook &Market::getOrderBook(string &sym)
{
    addOrderBook(sym);
    auto &o = *booksBySymbol.at(sym);
    return o;
}