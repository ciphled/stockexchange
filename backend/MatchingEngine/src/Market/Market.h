#ifndef _MARKET_H_
#define _MARKET_H_
#include <string>
#include <unordered_map>
#include "../OrderBook/OrderBook.h"
class Market
{
private:
    std::unordered_map<std::string, std::unique_ptr<OrderBook>> booksBySymbol;
    void addOrderBook(std::string &);

public:
    OrderBook &getOrderBook(std::string &);
};

#endif //_MARKET_H_
