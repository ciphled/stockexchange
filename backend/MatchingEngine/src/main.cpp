#include <iostream>
#include <string>
#include <thread>

#include "Market/Market.h"
#include "OrderBook/OrderBook.h"

#include "Exceptions/NoSuchSideException/NoSuchSideException.h"

#include <zmq.hpp>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using namespace std;

void handleAdd(Market &market, json &j)
{
    string sym = j["Sym"];
    string side = j["Side"];
    auto id = j["Id"].get<uint64_t>();
    auto price = j["Price"].get<int>();
    auto size = j["Size"].get<uint64_t>();

    OrderBook &ob = market.getOrderBook(sym);

    ob.addOrder(side.at(0), id, price, size);

    cout << ob << endl;
}


void handleOrderRequest(Market &market, string &orderJson)
{
    json j = json::parse(orderJson);
    string type = j["Type"];
    if (type == "A")
    {
        handleAdd(market, j);
    }
}

string handlePriceRequest(Market &market, string &orderJson)
{
    json j = json::parse(orderJson);
    string sym = j["Sym"];
    OrderBook &ob = market.getOrderBook(sym);

    json response;
    response["Price"] = ob.getPrice();
    response["Volume"] = ob.getVolume();

    return response.dump();
}


void pricing(Market &market)
{
    // initialize the zmq context with a single IO thread
    zmq::context_t context{1};

    // construct a REP (reply) socket and connect to interface
    zmq::socket_t socket{context, zmq::socket_type::rep};
    socket.bind("tcp://*:6666");

    try
    {
        while (true)
        {
            zmq::message_t request;
            // receive JSON request from client
            socket.recv(request, zmq::recv_flags::none);
            auto orderJson = request.to_string();

            std::cout << "Received " << orderJson << std::endl;
            // Process the order json
            const string response = handlePriceRequest(market, orderJson);

            // send JSON reply to the client
            socket.send(zmq::buffer(response), zmq::send_flags::none);
        }
    }
    catch (json::parse_error &ex)
    {
        cerr << ex.what() << endl;
    }
}

void orders(Market &market)
{
    // initialize the zmq context with a single IO thread
    zmq::context_t context{1};

    // construct a REP (reply) socket and connect to interface
    zmq::socket_t socket{context, zmq::socket_type::rep};
    socket.bind("tcp://*:5555");

    try
    {
        while (true)
        {
            zmq::message_t request;
            // receive JSON request from client
            socket.recv(request, zmq::recv_flags::none);
            auto orderJson = request.to_string();

            std::cout << "Received " << orderJson << std::endl;
            // Process the order json
            handleOrderRequest(market, orderJson);

            const string data{"done"};
            // send JSON reply to the client
            socket.send(zmq::buffer(data), zmq::send_flags::none);
        }
    }
    catch (NoSuchSideException &ex)
    {
        cerr << ex.what() << endl;
    }
    catch (json::parse_error &ex)
    {
        cerr << ex.what() << endl;
    }
}

int main(int argc, char **argv)
{

    Market market;

    // Spawn workers
    thread pricingHandler(pricing, ref(market));
    thread ordersHandler(orders, ref(market));

    // Wait til done
    pricingHandler.join();
    ordersHandler.join();

    return 0;
}