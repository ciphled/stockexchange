#ifndef _I_PRINTABLE_H_
#define _I_PRINTABLE_H_

#include <iostream>
/*
Any implementing class overloads << for printing
*/
class I_Printable
{
    friend std::ostream &operator<<(std::ostream &, const I_Printable &);

public:
    virtual void print(std::ostream &) const = 0;
};

#endif // _I_PRINTABLE_H_