#include "I_Printable.h"

using namespace std;
ostream &operator<<(ostream &os, const I_Printable &ip)
{
    ip.print(os);
    return os;
}
