#ifndef _ORDER_BOOK_H
#define _ORDER_BOOK_H

#include <unordered_map>
#include <map>
#include <set>
#include "../Order/Order.h"

class OrderBook : public I_Printable
{
private:
    std::string symbol;
    uint64_t totalVolume;
    std::multiset<Order> buys;
    std::unordered_map<uint64_t, std::multiset<Order>::iterator> buyOrders;
    std::multiset<Order> sells;
    std::unordered_map<uint64_t, std::multiset<Order>::iterator> sellOrders;

public:
    OrderBook(std::string symbol);
    void print(std::ostream &) const override;
    std::multiset<Order> &getQueue(char side);
    std::unordered_map<uint64_t, std::multiset<Order>::iterator> &getOrders(char side);
    void addOrder(char side, uint64_t id, int price, uint64_t vol);
    void process();
    std::string getSymbol();
    std::multiset<Order>::iterator &getOrder(char side, uint64_t id);
    void addOrder(Order);
    void removeOrder(uint64_t, char);
    int getPrice();
    uint64_t getVolume();
};

#endif