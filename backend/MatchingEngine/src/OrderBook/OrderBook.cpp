#include "OrderBook.h"
#include <iomanip>
#include "../Exceptions/NoSuchSideException/NoSuchSideException.h"

using namespace std;

OrderBook::OrderBook(std::string symbol)
    : symbol(symbol), totalVolume(0) {}

multiset<Order> &OrderBook::getQueue(char side)
{
    if (side == 'B')
    {
        return buys;
    }
    else if (side == 'S')
    {
        return sells;
    }
    throw NoSuchSideException();
}

std::unordered_map<uint64_t, std::multiset<Order>::iterator> &OrderBook::getOrders(char side)
{
    if (side == 'B')
    {
        return buyOrders;
    }
    else if (side == 'S')
    {
        return sellOrders;
    }
    throw NoSuchSideException();
}

void OrderBook::process()
{
    if (buys.size() == 0 || sells.size() == 0)
    {
        return;
    }
    // Process fulfilment until queues are stable
    while (buys.size() > 0 && sells.size() > 0)
    {
        // Get top
        Order bestbuy = *buys.begin();
        Order bestsell = *sells.begin();

        if (bestbuy.price < bestsell.price)
        {
            break;
        }
        uint64_t takevol = min(bestbuy.volume, bestsell.volume);

        removeOrder(bestbuy.id, 'B');
        removeOrder(bestsell.id, 'S');

        if (bestbuy.volume - takevol > 0)
        {
            // Best buy hasn't been fully filled. So add back.
            Order order{bestbuy};
            order.setVolume(bestbuy.volume - takevol);
            addOrder(order);
        }
        if (bestsell.volume - takevol > 0)
        {
            // Best sell hasn't been fully filled. So add back.
            Order order{bestsell};
            order.setVolume(bestsell.volume - takevol);
            addOrder(order);
        }
    }
}

void OrderBook::addOrder(char side, uint64_t id, int price, uint64_t vol)
{
    // Get existing order vol if any
    auto &map = getOrders(side);
    auto &mset = getQueue(side);
    uint64_t newvol = vol;
    if (map.find(id) != map.end())
    {
        auto it = getOrder(side, id);
        newvol += (*it).volume;
        removeOrder(id, side);
    }
    // Instantiate order with updated vol
    Order order(symbol, side, id, newvol, price);

    // Enqueue and update map
    addOrder(order);
    // Process fulfilment under queues are stable
    process();
}

void OrderBook::addOrder(Order order)
{
    // Adds order to mset and updates id->it map
    totalVolume += order.volume;
    auto &map = getOrders(order.side);
    auto &mset = getQueue(order.side);

    auto it = mset.insert(order);
    map.emplace(order.id, it);
}

void OrderBook::removeOrder(uint64_t id, char side)
{
    // Removes order and updates id->it map
    auto &map = getOrders(side);
    auto &mset = getQueue(side);
    auto it = getOrder(side, id);
    totalVolume -= (*it).volume;
    mset.erase(it);
    map.erase(id);
}


multiset<Order>::iterator &OrderBook::getOrder(char side, uint64_t id)
{
    auto &orderMap = getOrders(side);
    return orderMap.find(id)->second;
}

int OrderBook::getPrice()
{
    if (sells.size() + buys.size() == 0)
    {
        return 10; // base price in the case of no orders.
    }
    if (sells.size() == 0)
    {
        auto bestbuy = *buys.begin();
        return bestbuy.price;
    }
    auto bestsell = *sells.begin();
    return bestsell.price;
}

uint64_t OrderBook::getVolume()
{
    return totalVolume;
}

// Print orderbook as a neat table
void OrderBook::print(ostream &os) const
{
    // Header
    os << setw(61) << setfill('-') << "";
    os << symbol;
    os << setw(61) << setfill('-') << "" << endl;
    os << setfill(' ');
    os << setw(17 * 2) << "BUY" << setw(15 * 2 - 1) << "|";
    os << setw(18 * 2) << "SELL" << setw(14 * 2) << endl;
    os << setw(61 * 2 + 3) << setfill('-') << "" << endl;
    os << setfill(' ');

    // Body column names
    os << setw(11 * 2) << left << "Order Id";
    os << setw(10 * 2) << left << "Volume";
    os << setw(10 * 2) << right << "Price";
    os << "|";
    os << setw(11 * 2) << left << "Price";
    os << setw(10 * 2) << right << "Volume";
    os << setw(10 * 2) << right << "Order Id";
    os << endl;
    // Body (orders)
    auto sellIt = sells.begin();
    auto buyIt = buys.begin();
    while (sellIt != sells.end() || buyIt != buys.end())
    {
        if (buyIt != buys.end())
        {
            Order order = *buyIt;
            os << setw(11 * 2) << left << order.getId();
            os << setw(10 * 2) << left << order.getVolume();
            os << setw(10 * 2) << right << order.getPrice();
            os << "|";
            buyIt++;
        }
        else
        {
            os << setw(32 * 2) << right << "|";
        }
        if (sellIt != sells.end())
        {
            Order order = *sellIt;
            os << setw(11 * 2) << left << order.getPrice();
            os << setw(10 * 2) << right << order.getVolume();
            os << setw(10 * 2) << right << order.getId();
            sellIt++;
        }
        else
        {
            os << setw(31 * 2) << "";
        }
        os << endl;
    }
    os << endl;
}

string OrderBook::getSymbol()
{
    return symbol;
}