# C++ Matching Engine

## Build

1. Install dependencies for header only libraries [nlohmann/json](https://github.com/nlohmann/json) and [zeromq/cppzmq](https://github.com/zeromq/cppzmq). See their respective docs as linked.
2. Run `cmake CMakeLists.txt` in `backend/MatchingEngine/src` to generate the makefiles
3. Run `make` to build from the generated makefiles

## Run

```
./MatchingEngine
```
