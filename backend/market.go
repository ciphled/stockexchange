package main

type Market struct {
	matchingEngines map[string]*MatchingEngine
}

func (mkt *Market) addMatchingEngine(sym string) {
	me := newMatchingEngine(sym)
	go me.run()
	mkt.matchingEngines[sym] = me
}

func (mkt *Market) getMatchingEngine(sym string) *MatchingEngine {
	return mkt.matchingEngines[sym]
}
