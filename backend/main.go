package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

var tickers = []string{
	"AAPL",
	"MSFT",
}

const (
	addr = ":8080"
)

func makeHandler(
	fn func(mkt *Market, w http.ResponseWriter, r *http.Request),
	mkt *Market,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(mkt, w, r)
	}
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func handleOrders(mkt *Market, w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	switch r.Method {
	case "POST":
		fmt.Println("Trying to post", r.Body)
		// Get the payload from response
		var order Order
		err := json.NewDecoder(r.Body).Decode(&order)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// Get the corresponding matching engine
		me := mkt.getMatchingEngine(order.Sym)
		// Block until it's read by the ME
		me.order <- &order
	default:
		fmt.Fprintf(w, "You can only post orders.")
	}
}

func main() {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	market := &Market{
		matchingEngines: make(map[string]*MatchingEngine),
	}

	for _, sym := range tickers {
		market.addMatchingEngine(sym)
	}

	http.HandleFunc("/ws", makeHandler(handleChart, market))
	http.HandleFunc("/orders", makeHandler(handleOrders, market))

	err := http.ListenAndServe(addr, nil)
	if err != nil {
		log.Fatal("HTTP ListenAndServe:", err)
	}
}
