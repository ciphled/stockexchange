package main

import (
	"encoding/json"
	"fmt"
	"time"

	zmq "github.com/pebbe/zmq4"
)

type State struct {
	Price  int
	Volume int
	Time   int64
}

type PriceRequest struct {
	Sym string
}

type MatchingEngine struct {
	clients     map[*Client]bool
	register    chan *Client
	unregister  chan *Client
	order       chan *Order
	state       *State
	sym         string
	orderSocket *zmq.Socket
	priceSocket *zmq.Socket
}

func (me *MatchingEngine) handleRegistry() {
	for {
		select {
		case client := <-me.register:
			me.clients[client] = true
		case client := <-me.unregister:
			_, ok := me.clients[client]
			if ok {
				delete(me.clients, client)
				close(client.send)
			}
		}
	}
}

func (me *MatchingEngine) addOrder(order *Order) {
	// Send order to MatchingEngine for processing
	s := me.orderSocket
	orderJson, _ := json.Marshal(order)
	fmt.Printf("Sending request %s...\n", string(orderJson))

	s.Send(string(orderJson), 0)
	// Wait for response.
	msg, _ := s.Recv(0)
	fmt.Printf("Received reply [ %s ]\n", msg)
}

func (me *MatchingEngine) handleOrders() {
	for order := range me.order {
		me.addOrder(order)
	}
}

func (me *MatchingEngine) handleStateUpdate() {

	ticker := time.NewTicker(200 * time.Millisecond)
	s := me.priceSocket
	for range ticker.C {
		// broadcast price/vol state to all clients
		payload := PriceRequest{Sym: me.sym}

		payloadJson, _ := json.Marshal(payload)
		fmt.Printf("Sending request %s...\n", string(payloadJson))

		s.Send(string(payloadJson), 0)

		// Wait for response.
		msg, _ := s.Recv(0)
		fmt.Printf("Received reply [ %s ]\n", msg)

		var state State
		err := json.Unmarshal([]byte(msg), &state)
		if err != nil {
			fmt.Printf("Couldn't parse state [ %s ]\n", msg)
			return
		}
		state.Time = time.Now().UnixNano() / 1e6

		// Broadcast state to clients
		for client := range me.clients {
			select {
			case client.send <- &state:
			default:
				fmt.Println("Closing send")
				close(client.send)
				delete(me.clients, client)
			}
		}
	}
}

func newMatchingEngine(sym string) *MatchingEngine {
	zctx1, _ := zmq.NewContext()
	zctx2, _ := zmq.NewContext()

	orderSocket, _ := zctx1.NewSocket(zmq.REQ)
	orderSocket.Connect("tcp://localhost:5555")

	priceSocket, _ := zctx2.NewSocket(zmq.REQ)
	priceSocket.Connect("tcp://localhost:6666")

	return &MatchingEngine{
		clients:     make(map[*Client]bool),
		register:    make(chan *Client),
		unregister:  make(chan *Client),
		order:       make(chan *Order),
		state:       &State{Price: 100, Volume: 0},
		sym:         sym,
		orderSocket: orderSocket,
		priceSocket: priceSocket,
	}
}

func (me *MatchingEngine) run() {
	go me.handleRegistry()
	go me.handleOrders()
	go me.handleStateUpdate()
}
